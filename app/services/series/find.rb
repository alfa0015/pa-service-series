module Series
    class Find < ::ApplicationService
        attr_reader :id
        def initialize(id)
            @id = id
        end
        def call
            serie = Serie.find(@id)
            success(serie)
            rescue ActiveRecord::RecordNotFound
                return error_not_found
        end
    end
end