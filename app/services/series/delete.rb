module Series
    class Delete < ::ApplicationService
        attr_reader :id
        def initialize(id)
            @id = id
        end
        def call
            serie = ::Series::Find.call(@id)
            if serie.succeed?
                begin
                    serie.response.destroy!
                    success(serie.response)
                rescue ActiveRecord::RecordNotDestroyed
                    return error_invalid_record(serie.response.errors.to_h)
                end
            else
                return error_not_found
            end
            
        end
    end
end