class ApplicationService
  def self.call(*args, &block)
    new(*args, &block).call
  end
  def success(response = Object)
    SuccessService.new(response)
  end
  def error
    ErrorService.new
  end
  def error_not_found
    ErrorService.new(
      title: "Record not Found",
      status: 404,
      detail: "We could not find the object you were looking for.",
      source: { pointer: "/request/url/:id" }
    )
  end
  def error_invalid_record(errors)
    ErrorServiceInvalid.new(errors)
  end
end

class SuccessService
  attr_reader :response
  def initialize(response)
    @response = response
  end
  def succeed?
    true
  end
end

class ErrorService
  attr_reader :title, :detail, :status, :source
  def initialize(title: nil, detail: nil, status: nil, source: {})
    @title = title || "Something went wrong"
    @detail = detail || "We encountered unexpected error, but our developers had been already notified about it"
    @status = status || 500
    @source = source.deep_stringify_keys
    def to_h
      {
        status: status,
        title: title,
        detail: detail,
        source: source
      }
    end
    def serializable_hash
      to_h
    end
    def to_s
      to_h.to_s
    end
    def succeed?
      false
    end
  end
end

class ErrorServiceInvalid
  attr_reader :errors
  def initialize(errors)
    @errors = errors
    @status = 422
    @title = "Unprocessable Entity"
  end

  def serializable_hash
    errors.reduce([]) do |r, (att, msg)|
      r << {
        status: @status,
        title: @title,
        detail: msg,
        source: { pointer: "/data/attributes/#{att}" }
      }
    end
  end

  def succeed?
    false
  end
end