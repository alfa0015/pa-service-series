class SerieSerializer
  include FastJsonapi::ObjectSerializer
  attributes :name
  attributes :description
end
