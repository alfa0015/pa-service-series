module Api
    module V1
        class SeriesController < ApplicationController
            def index
                series = Serie.order(:id)
                render json: SerieSerializer.new(series).serialized_json
            end
            def show
                result = ::Series::Find.call(params[:id])
                if result.succeed?
                    render json: SerieSerializer.new(result.response).serialized_json
                else
                    render json: ErrorSerializer.new(result), status: result.status
                end
            end
            def create
                result = ::Series::Create.call(serie_params)
                if result.succeed?
                    render json: {}, status: :no_content
                else
                    render json: ErrorSerializer.new(result), status: :unprocessable_entity
                end
            end
            def update
                result = ::Series::Update.call(params[:id],serie_params)
                if result.succeed?
                    render json: {}, status: :no_content
                else
                    render json: ErrorSerializer.new(result), status: :unprocessable_entity
                end
            end
            def destroy
                result = ::Series::Delete.call(params[:id])
                if result.succeed?
                    render json: {}, status: :no_content
                else
                    render json: ErrorSerializer.new(result), status: :unprocessable_entity
                end
            end
            private
                def serie_params
                    params.permit(:name, :description)
                end
        end
    end
end