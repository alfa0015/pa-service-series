module Series
    class Create < ::ApplicationService
        attr_reader :params
        def initialize(params)
            @params = params
        end
        def call
            serie = Serie.new(params)
            serie.save!
            success(serie)
            rescue ActiveRecord::RecordInvalid
                return error_invalid_record(serie.errors.to_h)
        end
    end
end