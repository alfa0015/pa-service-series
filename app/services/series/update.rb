module Series
    class Update < ::ApplicationService
        attr_reader :id, :params
        def initialize(id, params)
            @id = id
            @params = params
        end
        def call
            serie = ::Series::Find.call(@id)
            if serie.succeed?
                begin
                    serie.response.update!(@params)
                    success(serie.response)
                rescue ActiveRecord::RecordInvalid
                    return error_invalid_record(serie.response.errors.to_h)
                end
            else
                return error_not_found
            end
            
        end
    end
end